# 开源企业考勤系统ClockSimpleJEE

#### 项目介绍
ClockSimpleJEE4 0.98测试案例集自动数据库链接回收版

ClockSimpleJEE4 0.98是经过仔细测试的105个测试案例可以完美运行的版本。 
这是Readme 

ClockSimpleJEE4是开源例程企业考勤系统 

用于Java,mysql学习。 

所有程序在GPLv3条款下开源，关于GPLv3相关pdf已放置在doc子目录下。
 
数据库脚本在sql子目录下。请恢复名为clock的产品库和名为clock_test的测试库。 

clock_test是用于单元测试的JUnit套件的测试空库。
 
请启动测试套件，享受105个测试方法编织的绵密的测试网。

可以启动JUnitEE测试套件 http://localhost:8080/clocksimplejee4/TestServlet 

doc下另有所有版本的release note和部分截图。 

本软件力推如下的JUnit测试黄金法则：在测试空库上无限次运行不出错。 
bug报告jerry_shen_sjf@qq.com 

用如下管理员登录 

用户名：160208 

密码：jerry 

系统截图：
登录页
![输入图片说明](https://gitee.com/uploads/images/2018/0524/214202_30e28cc3_1203742.png "cover.png")

内页：
![输入图片说明](https://gitee.com/uploads/images/2018/0524/214231_1d6c8a8a_1203742.png "inner.png")

#### 软件架构
软件架构说明
jsp,Servlet, jdbc, mysql


#### 错误修复
现在，数据库后端换成了mariadb,　和原来的mysql版本不同的是，现在表名是大小写区分的，原来的程序会出现错误。现在，我已经修复了这些错误。在Java8,Tomcat 8.5和mariadb 10的环境下可以无错运行。同时，我还修复了系统内JUnitEE的105个测试案例。运行截图如下：

![Image description](https://images.gitee.com/uploads/images/2021/0519/223348_4a101c56_1203742.png "clc_testcase.png")

![Image description](https://images.gitee.com/uploads/images/2021/0519/223410_396ea699_1203742.png "clc_test_result.png")